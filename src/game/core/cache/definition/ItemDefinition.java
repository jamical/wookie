package game.core.cache.definition;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Represents the definition of an item.
 * @author jamix77
 *
 */
public class ItemDefinition {
	
	/**
	 * List of definitions.
	 */
	public static Map<Integer,ItemDefinition> definitions = new LinkedHashMap<Integer,ItemDefinition>();
	
	private int id;
	private String name;
	private String description;
	
	/**
	 * Constructs a new {@code ItemDefnition}.
	 * @param id
	 * @param name
	 * @param description
	 */
	public ItemDefinition(int id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	public void put() {
		definitions.put(id, this);
	}
	
	public static ItemDefinition forId(int id) {
		return definitions.get(id);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
}
