package game.core.cache;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import game.core.cache.definition.ItemDefinition;

/**
 * For everything to do with cache. Including parsing definitions.
 * @author jamix77
 *
 */
public class Cache {

	private File root;
	
	private List<ItemDefinition> itemDefs = new LinkedList<ItemDefinition>();
	
	/**
	 * Constructs a new {@code Cache} with params of root.
	 * @param root
	 */
	public Cache(File root) {
		this.root = root;
	}
	
	/**
	 * Read from a cache index.
	 * @param idx
	 * @throws IOException 
	 */
	public void read(int idx) throws IOException {
		if (idx == 0) {
			readItems();
		}
		
	}
	
	/**
	 * Parse item definitions.
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public void readItems() throws NumberFormatException, IOException {
		String oldStr;
		String newStr;
		BufferedReader reader = new BufferedReader(new FileReader(new File(root,"cache_idx0")));
		int i = 0;
		ItemDefinition current =null;
		while ((oldStr = reader.readLine()) != null) {
			if (oldStr.startsWith("//")){
				continue;
			}
			if (i % 3 == 0) {
				if (current != null) {
					getItemDefs().add(current);
					current.put();
				}
				current = new ItemDefinition(-1,null,null);
			}
			i++;
			newStr = convert(oldStr);
			if (newStr.equalsIgnoreCase("end")) {
				break;
			}
			if (current.getId() == -1) {
				current.setId(Integer.parseInt(newStr));
				continue;
			}
			if (current.getName() == null) {
				current.setName(newStr);
				continue;
			}
			if (current.getDescription() == null) {
				current.setDescription(newStr);
				continue;
			}
		}
		reader.close();
	}
	
	/**
	 * Convert binary to regular string.
	 * @param unconv
	 * @return
	 */
	public String convert(String unconv) {
		String[] splitted = unconv.split(" ");
		List<String> j = new LinkedList<String>();
		for (String input : splitted) {
		StringBuilder sb = new StringBuilder(); // Some place to store the chars

		Arrays.stream( // Create a Stream
		    input.split("(?<=\\G.{8})") // Splits the input string into 8-char-sections (Since a char has 8 bits = 1 byte)
		).forEach(s -> // Go through each 8-char-section...
		    sb.append((char) Integer.parseInt(s, 2)) // ...and turn it into an int and then to a char
		);

		String output = sb.toString(); // Output text (t)
		j.add(output);
		}
		StringBuilder builder = new StringBuilder();
		j.forEach(str -> builder.append(str));
		return builder.toString();
	}

	public List<ItemDefinition> getItemDefs() {
		return itemDefs;
	}

	public void setItemDefs(List<ItemDefinition> itemDefs) {
		this.itemDefs = itemDefs;
	}

}
