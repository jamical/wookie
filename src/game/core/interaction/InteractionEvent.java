package game.core.interaction;

import game.core.node.Node;

/**
 * Represents an interaction event.
 * This will be an event when an interaction is done. This is abstract
 * so it can be changed and modified as pleased.
 * @author jamix77
 *
 */
public abstract class InteractionEvent {
	
	private Node[] nodes = new Node[2];

	/**
	 * Execute whatever happens during this event.
	 */
	public abstract void execute();

	public Node[] getNodes() {
		return nodes;
	}

	public void setNodes(Node[] nodes) {
		this.nodes = nodes;
	}
	
}
