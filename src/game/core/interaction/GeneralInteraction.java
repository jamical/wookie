package game.core.interaction;

import java.util.LinkedHashMap;
import java.util.Map;

import game.core.node.Node;
import game.core.plugin.Plugin;
import game.core.plugin.PluginManifest;
import game.core.plugin.PluginType;

/**
 * A general interaction usage event class.
 * @author jamix77
 *
 */
@PluginManifest(PluginType.INTERACTION)
public abstract class GeneralInteraction implements Plugin<Node> {
	
	/**
	 * Interactions list.
	 */
	public static final Map<String,GeneralInteraction> interactions = new LinkedHashMap<String,GeneralInteraction>();
	
	/**
	 * General interactions :)
	 * @param identifier
	 * @param inter
	 */
	public static void bind(String identifier, GeneralInteraction inter) {
		interactions.put(identifier, inter);
	}
	
	/**
	 * Execute the action.
	 */
	public abstract void execute(InteractionEvent event);

}
