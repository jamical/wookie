package game.core.container;

import java.util.Deque;
import java.util.concurrent.LinkedBlockingDeque;

import game.core.node.impl.item.Item;

/**
 * Represents a container that contains items.
 * @author jamix77
 *
 */
public abstract class Container {
	
	/**
	 * The capacity of this container.
	 * @return
	 */
	public abstract int capacity();
	
	/**
	 * A deque of items that are in this container.
	 */
	public Deque<Item> items = new LinkedBlockingDeque<Item>();

}
