package game.core.container.impl;

import game.core.container.Container;

/**
 * 
 * @author jamix77
 *
 */
public class Inventory extends Container {

	@Override
	public int capacity() {
		return 28;
	}

}
