package game.core.plugin;

/**
 * Represents a generic plugin.
 * @author jamix77
 *
 */
public interface Plugin<T> {
	
	/**
	 * Create a new instance of this.
	 * @param arg the argument
	 * @return the new instance.
	 */
	public Plugin<T> newInstance(T arg);
	
	/**
	 * Indentifier to indentify what
	 * what to get from the map.
	 * @return
	 */
	public String identifier();

}
