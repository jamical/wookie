package game.core.plugin;

/**
 * Represents a type of plugin in an enumeration.
 * @author jamix77
 *
 */
public enum PluginType {
	
	/**
	 * A pulse. Represented as an enumerator in this enumeration.
	 */
	PULSE,
	
	/**
	 * An interaction. Represented as an enumerator in this enumeration.
	 */
	INTERACTION;

}
