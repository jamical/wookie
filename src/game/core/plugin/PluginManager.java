package game.core.plugin;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

import game.Main;
import game.core.interaction.GeneralInteraction;
import game.core.pulse.Pulse;

/**
 * The manager for the plugins.
 * @author jamix77
 *
 */
public class PluginManager {
	
	/**
	 * The amount of plugins loaded.
	 */
	private static int plugins;
	
	/**
	 * All loaded plugins stored here along with their identifiers.
	 */
	private static final Map<String,Plugin<?>> pluginsLoaded = new LinkedHashMap<String,Plugin<?>>();
	
	/**
	 * The loaded pulses.
	 */
	private static final List<Pulse> pulses = new LinkedList<Pulse>();
	
	/**
	 * The loaded interactions.
	 */
	private static final List<GeneralInteraction> interactions = new LinkedList<GeneralInteraction>();
	
	/**
	 * Logger instance.
	 */
	private static Logger logger = Logger.getLogger(PluginManager.class.getName());
	
	/**
	 * Initilise the loading of the plugins
	 */
	public static void init() {
		try {
			load(new File("./plugins/"));
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("Initilized " + plugins + " plugins.");
	}
	
	/**
	 * 
	 * @param directory
	 * @throws Throwable
	 */
	private static void load(File directory) throws Throwable {
		final URL[] url = new URL[] {directory.toURI().toURL(),null};
		URLClassLoader loader;
		for (File file : directory.listFiles()) {
			if (file.isDirectory()) {
				load(file);
				continue;
			}
			String fileName = file.getName().replaceAll(".jar", "").replace(".class", "");
			url[1] = file.toURI().toURL();
			loader = new URLClassLoader(url);
			JarFile jar = new JarFile(file);
			Enumeration<JarEntry> entries = jar.entries();
			boolean loaded = false;
			while (entries.hasMoreElements()){
				JarEntry entry = entries.nextElement();
				if (entry.getName().endsWith(fileName+".class")) {
					StringBuilder sb = new StringBuilder();
					for (String path : entry.getName().split("/")) {
						if (sb.length() != 0) {
							sb.append(".");
						}
						sb.append(path);
						if (path.endsWith(".class")) {
							sb.setLength(sb.length() - 6);
							break;
						}
					}
					try {
						final Plugin<?> plugin = (Plugin<?> ) loader.loadClass(sb.toString()).newInstance();
						define(plugin);
						loaded = true;
					} catch (Throwable t) {
						t.printStackTrace();
					}
				}
			}
			if (!loaded) {
				System.err.println("Failed to load plugin " + fileName);
			}
			jar.close();
		}
	}
	
	/**
	 * Define the plugin and add it to the specified list.
	 * @param plugin
	 */
	private static void define(Plugin<?> plugin) {
		PluginManifest manifest = plugin.getClass().getAnnotation(PluginManifest.class);
		if (manifest == null) {
			manifest = plugin.getClass().getSuperclass().getAnnotation(PluginManifest.class);
		}
		if (manifest == null) {
			return;
		} 
		if (manifest.value() == null) {
			return;
		}
		else if (manifest.value() == PluginType.PULSE) {
			pulses.add(((Pulse)plugin));
		}
		else if (manifest.value() == PluginType.INTERACTION) {
			interactions.add((GeneralInteraction)plugin);
		}
		else {
			System.out.println("Manifest: " + manifest.value());
		}
		plugin.newInstance(null);
		pluginsLoaded.put(plugin.identifier(), plugin);
		plugins++;
	}
	
	/**
	 * Execute whatever plugin can be found simply by the identifier.
	 * @param identifier
	 */
	public static void execute(String identifier) {
		Plugin<?> plugin = pluginsLoaded.get(identifier);
		if (plugin != null) {
			if (plugin instanceof Pulse) {
				Main.mainLoop.submit((Pulse)plugin);
			}
		}
	}

}
