package game.core.plugin;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Represents a plugin manifest.
 * @author jamix77
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface PluginManifest {

	/**
	 * Gets the plugin type.
	 * @return The plugin type.
	 */
	public PluginType value();
	
}