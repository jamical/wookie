package game.core.node.impl.item;

import game.core.cache.definition.ItemDefinition;
import game.core.node.Node;

/**
 * Represents an item that a player can have on them.
 * @author jamix77
 *
 */
public class Item {
	
	/**
	 * The id of the item.
	 */
	private int id;
	
	/**
	 * The amount of the item.
	 */
	private int amount;
	
	/**
	 * Constructs a new {@code Item}.
	 * @param id
	 */
	public Item(int id) {
		this(id,1);
	}
	
	/**
	 * Constructs a new {@code Item}.
	 * @param id
	 * @param amount
	 */
	public Item(int id, int amount) {
		if (amount < 0) {
			throw new IllegalArgumentException("You cannot have a negative amount of an item.");
		}
		this.id = id;
		this.amount = amount;
	}
	
	/**
	 * Get the item definition for this specified item.
	 * @return
	 */
	public ItemDefinition getDefinition() {
		return ItemDefinition.forId(id);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

}
