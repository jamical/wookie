package game.core.node.impl;

import java.util.Deque;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Represents a handler for the movement of a {@link MobileEntity}.
 * @author jamix77
 *
 */
public class MovementHandler {
	
	/**
	 * Waypoints that the game will process and move the character through.
	 */
	private Deque<Point> waypoints = new LinkedBlockingDeque<Point>();
	
	/**
	 * Represents a 2d point.
	 * @author jamix77
	 *
	 */
	public static class Point {
		
		/**
		 * The x and y points of this point.
		 */
		private int x,y;
		
		public void setX(int x) {
			this.x = x;
		}
		
		public void setY(int y) {
			this.y = y;
		}
		
		public int getX() {
			return x;
		}
		
		public int getY() {
			return y;
		}
		
	}

	public Deque<Point> getWaypoints() {
		return waypoints;
	}

	public void setWaypoints(Deque<Point> waypoints) {
		this.waypoints = waypoints;
	}

}
