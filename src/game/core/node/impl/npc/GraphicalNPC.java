package game.core.node.impl.npc;

import game.core.Position;
import game.core.node.GraphicalNode;

/**
 * Represents the graphics of the npc.
 * @author jamix77
 *
 */
public class GraphicalNPC extends GraphicalNode {

	public GraphicalNPC(String string,Position position) {
		super(string,position);
	}

	

}
