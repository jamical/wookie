package game.core.node.impl;

import game.core.Position;

/**
 * A entity that can move.
 * @author jamix77
 *
 */
public class MobileEntity {
	
	/**
	 * Every node will have a position.
	 */
	private final Position position;
	
	/**
	 * A handler for the movement.
	 */
	private final MovementHandler movementHandler = new MovementHandler();
	
	/**
	 * Constructs a new {@code MobileEntity}.
	 * @param position
	 */
	public MobileEntity(Position position) {
		this.position = position;
	}

}
