package game.core.node;

import game.core.Position;

/**
 * Represents an interactable node in the game.
 * @author jamix77
 *
 */
public class Node {
	
	/**
	 * The index position this node is in.
	 */
	private int index;
	
	/**
	 * Constructs {@code Node}.
	 * @param position
	 */
	public Node(int index) {
		this.index = index;
	}


	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	
}
