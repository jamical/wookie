package game.core.node;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import game.core.Position;

/**
 * Represents a node (graphically).
 * @author jamix77
 *
 */
public class GraphicalNode extends ImageIcon {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9024820505185406572L;
	
	/**
	 * The label that this is contained in.
	 */
	private JLabel label = new JLabel(this);
	
	private Position position;
	
	public GraphicalNode(String string, Position position) {
		super(string);
		this.setPosition(position);
		label = new JLabel(this);
	}
	public JLabel getAsLabel() {
		if (label == null) {
			label = new JLabel(this);
		}
		return label;
	}
	
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}


}
