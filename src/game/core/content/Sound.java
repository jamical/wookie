package game.core.content;

import game.core.context.Contextable;
import game.core.context.SoundContext;

/**
 * Represents a single sound in the game.
 * @author jamix77
 *
 */
public class Sound {
	
	/**
	 * The sound context that this sound follows
	 */
	private final SoundContext context;
	
	/**
	 * Play the sound.
	 */
	public void play() {
		if (context == null || context.getArgs() == null || context.getArgs().length <= 0)
			throw new NullPointerException("Context is an empty context.");
		
		int idx = getIdx();
		//TODO XXX
		
		
	}
	
	
	/**
	 * Constructs a new {@code Sound} with arguments of idx.
	 * @param idx
	 */
	public Sound(Contextable context) {
		this.context = (SoundContext) context;
	}
	
	public int getIdx() {
		return (int) context.getArgs()[0];
	}
	
	

}
