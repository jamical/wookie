package game.core.pulse;

import game.Main;
import game.core.Position;
import game.core.node.GraphicalNode;
import game.core.plugin.Plugin;

/**
 * An example pulse.
 * Remove if you want. Ask me if your confused about anything
 * @author jamix77
 *
 */
public class TestPulse extends Pulse {

	int ticks = 0;
	
	GraphicalNode node;
	
	/**
	 * Execute the pulse every single tick {@value 500} milliseconds.
	 */
	@Override
	public void execute() {
		System.out.println(ticks);
		ticks++;
		if (node != null) {
			node.getPosition().setAs(new Position(ticks * 50,10));
			Main.getGameframe().repaint();
		}
		if (ticks == 3) {
			node = new GraphicalNode("bass_logo.png",new Position(50,50));
			Main.getGameframe().put(node);
		}
		if (ticks > 10) {
			Main.getGameframe().remove(node);
			remove = true;
			
		}
		
	}

	/**
	 * When we need remove this plugin from being run it will call this method.
	 */
	@Override
	public void onStop() {
		System.out.println("Called when the boolean 'remove' is set to {@code True}.");
	}

	@Override
	public Plugin<Object> newInstance(Object arg) {
		return new TestPulse();
	}

	@Override
	public String identifier() {
		return "TestPulseIdentifier";
	}

}
