package game.core.pulse;

import game.core.plugin.Plugin;
import game.core.plugin.PluginManifest;
import game.core.plugin.PluginType;

/**
 * A pulse which can be executed every single tick.
 * @author jamix77
 *
 */
@PluginManifest(PluginType.PULSE)
public abstract class Pulse implements Plugin<Object> {
	
	/**
	 * If the pulse is enabled.
	 */
	private boolean enabled = true;
	
	/**
	 * If the game should remove this from the list.
	 */
	protected boolean remove = false;
	
	/**
	 * Execute the pulse.
	 */
	public abstract void execute();
	
	/**
	 * When stopped this is called.
	 */
	public abstract void onStop();
	
	/**
	 * Stop the pulse.
	 */
	protected void stop() {
		enabled = false;
		remove = true;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isRemove() {
		return remove;
	}

	public void setRemove(boolean remove) {
		this.remove = remove;
	}

}
