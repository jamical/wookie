package game.core.context;

/**
 * A sounds context.
 * @author jamix77
 *
 */
public class SoundContext extends Contextable {

	@Override
	public int length() {
		return 1;
	}

}
