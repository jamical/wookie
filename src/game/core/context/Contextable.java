package game.core.context;

/**
 * Anything that can contain a context
 * @author jamix77
 *
 */
public abstract class Contextable {
	
	
	/**
	 * The length of the arguments
	 */
	public abstract int length();
	
	public Object[] getArgs() {
		return args;
	}

	public void setArgs(Object[] args) {
		this.args = args;
	}

	/**
	 * The index.
	 */
	private Object[] args = new Object[length()];
	
	

}
