package game.core;

/**
 * Represents a position in the game.
 * @author jamix77
 *
 */
public class Position {

	/**
	 * The x coordinate.
	 */
	private int x;
	
	/**
	 * The y coordinate.
	 */
	private int y;
	
	/**
	 * The z coordinate.
	 */
	private int z;
	
	/**
	 * Constructs a new {@code Position}.
	 * @param x
	 * @param y
	 */
	public Position(int x, int y) {
		this(x,y,0);
	}
	
	/**
	 * Constructor for a position.
	 * @param x
	 * @param y
	 */
	public Position(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * Set this position as another.
	 */
	public void setAs(Position position) {
		this.x = position.x;
		this.y = position.y;
		this.z = position.z;
	}
	
	/**
	 * Clone this position to another.
	 */
	public Position clone() {
		return new Position(x,y,z);
	}
	
	/**
	 * Get the x coordinate.
	 * @return
	 */
	public int getX() {
		return x;
	}
	
	
	/**
	 * Get the y coordinate.
	 * @return
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Get the z coordinate.
	 * @return
	 */
	public int getZ() {
		return z;
	}
	
	/**
	 * Get this as a string for debugging.
	 */
	@Override
	public String toString() {
		return "[X,Y,Z] : [" + x + "," + y + "," + z + "]";
	}
	
	/**
	 * Get the hashcode.
	 */
	@Override
	public int hashCode() {
		return z << 30 | x << 15 | y;
	}
	
	
}
