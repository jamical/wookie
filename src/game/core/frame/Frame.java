package game.core.frame;

import java.awt.Component;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;

import game.Constants;
import game.core.node.GraphicalNode;

/**
 * The games frame that can present itself (graphically).
 * @author jamix77
 *
 */
public class Frame {
	
	private JFrame coreFrame;
	
	List<GraphicalNode> gnodes = new LinkedList<GraphicalNode>();
	
	/**
	 * Put this on the frame.
	 * @param gn
	 */
	public void put(GraphicalNode gn) {
		JLabel la = gn.getAsLabel();
		coreFrame.getContentPane().add(la);
		la.setBounds(gn.getPosition().getX(), gn.getPosition().getY(), gn.getIconWidth(), gn.getIconHeight());
		gnodes.add(gn);
		repaint();
	}
	
	/**
	 * Remove something from the frame.
	 * @param gn
	 */
	public void remove(GraphicalNode gn) {
		coreFrame.getContentPane().remove(gn.getAsLabel());
		gnodes.remove(gn);
		repaint();
	}
	
	public void repaint() {
		for (GraphicalNode node : gnodes) {
			node.getAsLabel().setBounds(node.getPosition().getX(), node.getPosition().getY(), node.getIconWidth(), node.getIconHeight());
		}
		coreFrame.repaint();
		coreFrame.revalidate();
		coreFrame.getContentPane().repaint();
		coreFrame.getContentPane().revalidate();
	}
	
	
	/**
	 * Build the frame with its core components and its core attributes.
	 */
	public void build() {
		if (coreFrame != null) {
			throw new IllegalStateException("Game frame was already created.");
		}
		coreFrame = new JFrame(Constants.NAME);
		coreFrame.setPreferredSize(Constants.SIZE);
		coreFrame.setVisible(true);
		coreFrame.setResizable(false);
		coreFrame.setLayout(null);
		coreFrame.pack();
		coreFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/**
	 * Get the core frame.
	 * @return the core frame.
	 */
	public JFrame getFrame() {
		return coreFrame;
	}

}
