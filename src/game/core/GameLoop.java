package game.core;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import game.Constants;
import game.core.pulse.Pulse;

/**
 * The games loop.
 * @author jamix77
 *
 */
public class GameLoop implements Runnable {
	
	/**
	 * The list of pulses to be processed.
	 */
	private final List<Pulse> pulses = new LinkedList<Pulse>();
	

	private int ticks = 0;
	
	@Override
	/**
	 * Runnable implementation
	 */
	public void run() {
		ticks++;
		List<Pulse> toRemove = new LinkedList<Pulse>();
		pulses.forEach(pulse -> {
			if (pulse.isEnabled())
				pulse.execute();
			if (pulse.isRemove()) {
				toRemove.add(pulse);
				pulse.onStop();
			}
		});
		pulses.removeAll(toRemove);
	}
	
	/**
	 * Submit a pulse to be executed and such.
	 * @param pulse
	 */
	public void submit(Pulse pulse) {
		pulses.add(pulse);
	}
	
	public void start(ScheduledExecutorService service) {
		service.scheduleAtFixedRate(this, 0, Constants.PROCESSING_RATE, TimeUnit.MILLISECONDS);
	}

}
