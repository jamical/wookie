package game;

import java.awt.Dimension;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * The games never changing constants.
 * @author jamix77
 *
 */
public class Constants {
	
	/**
	 * The processing rate for the game loop. {@value 500} milliseconds.
	 */
	public static final int PROCESSING_RATE = 500;
	
	/**
	 * The games name.
	 */
	public static final String NAME = "PikZel";
	
	/**
	 * The games build.
	 */
	public static final double BUILD = 0.1;

	/**
	 * The welcoming message.
	 */
	public static final String WELCOMING_MESSAGE = "Welcome to " + NAME + ".";
	
	/**
	 * Administrators.
	 */
	public static final List<String> ADMINISTRATORS = new LinkedList<String>(Arrays.asList("Jamie","Alex"));
	
	/**
	 * The size of the frame.
	 */
	public static final Dimension SIZE = new Dimension(800,450);
	
}
