package game;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Logger;

import game.core.GameLoop;
import game.core.cache.Cache;
import game.core.cache.definition.ItemDefinition;
import game.core.frame.Frame;
import game.core.plugin.PluginManager;

/**
 * Main class of the game.
 * @author jamix77
 *
 */
public class Main {
	
	/**
	 * The game loop that is executed by the {@code ScheduledExecutorService}.
	 * The game loop cycle rate is 500ms.
	 */
	public static final GameLoop mainLoop = new GameLoop();
	
	/**
	 * Logger instance.
	 */
	private static final Logger logger = Logger.getLogger(Main.class.getName());
	
	/**
	 * The games displayable window.
	 */
	private static final Frame gameFrame = new Frame();
	
	/**
	 * Main method. Entry point of the program.
	 * @param args
	 */
	public static void main(String args[]) {
		final ExecutorService serviceLoader = Executors.newSingleThreadExecutor(); // Executor to load our services.
		
		logger.info("Initilizing plugins...");
		serviceLoader.execute(() -> PluginManager.init());
		
		logger.info("Building frame.");
		serviceLoader.execute(() -> gameFrame.build());
		
		
		
		final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor(); // Executor to load our loop and other core services.
		
		mainLoop.start(executor);
		
		Cache cache = new Cache(new File("./cache/"));
		try {
			cache.read(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("Parsed " + cache.getItemDefs().size() + " item definition"+(cache.getItemDefs().size() == 1 ? "" : "s")+".");
		
		
		//Shutdown the loader as its not needed anymore.
		serviceLoader.shutdown();
		
		logger.info("Loaded " + Constants.NAME + ".");
		
		
		
	}

	public static GameLoop getMainloop() {
		return mainLoop;
	}

	public static Logger getLogger() {
		return logger;
	}

	public static Frame getGameframe() {
		return gameFrame;
	}
	
	


}
